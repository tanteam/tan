const webpack = require('webpack');
const path = require("path");
const FileManagerPlugin = require('filemanager-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin'); 
const cssnano = require('cssnano');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = function(env, argv) {
  /*
  * boolean variable for development mode
  */
  const devMode = argv.mode === 'development';

  return {
    watch: devMode,
    devtool: devMode ? 'source-maps' : 'eval',
    entry: {
      app: ["./src/js/app.js", "./src/sass/style.scss"]
    },
    output: {
      path: path.join(__dirname, "dist"),
      filename: 'js/[name].min.js'
    },
    module: {
      rules: [
        /*
        * Handle ES6 transpilation
        */
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        },
        /*
        * Handle SCSS transpilation
        */
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            { 
              loader: 'css-loader',
              options: {
                sourceMap: devMode,
                minimize: !devMode
              }
            },
            { 
              loader: 'sass-loader',
              options: {
                sourceMap: devMode
              }
            }
          ]
        },
        /*
        * Handle Fonts
        */
        {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
              {
                loader: 'file-loader',
                options: {
                  include: path.join(__dirname, "src/fonts"),
                  name: '[name].[ext]',
                  outputPath: 'fonts',
                  publicPath: '../fonts'
                }
            }
          ]
        },
        /*
        * Handle Images Referenced in CSS
        */
        {
          test: /\.(gif|png|jpe?g|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                include: path.join(__dirname, "src/images"),
                name: '[name].[ext]',
                outputPath: 'images',
                publicPath: '../images'
              }
            },
            'image-webpack-loader'
          ],
        }
      ]
    },
    /*
    * NOTE: Optimization will only run on production mode
    */
    optimization: {
      /*
      * Split imported npm packages into a single file
      */
      splitChunks: {
        cacheGroups: {
          commons: {
            name: 'vendors',
            test: /[\\/]node_modules[\\/]/,
            chunks: 'all'
          }
        }
      },
      minimizer: [
        /*
        * Minimize javascript
        */
        new UglifyJsPlugin(),
        
      ],
    },
    plugins: [
      /*
      * Automatically load jquery instead of having to import it everywhere
      */
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      }),
      /*
      * Extract app CSS and npm package CSS into two separate files
      */
      new MiniCssExtractPlugin({
        filename: 'css/[name].min.css',
        chunkFilename: 'css/[id].min.css'
      }),
      /*
        * Minimize CSS
        */
        new OptimizeCSSAssetsPlugin({
          cssProcessor: cssnano,
          cssProcessorOptions: { discardComments: { removeAll: true } }
        }),
      /*
      * Using rimraf on build in package.json to first clean up the images folder
      * On the end of the build, copy all images to the dist folder
      */
      new FileManagerPlugin({
        onEnd: {
          copy: [
           // { source: './src/images', destination: './dist/images' },
          ]
        }
      })
    ]
  }
};