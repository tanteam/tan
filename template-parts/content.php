<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theabhinews.in
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	<?php
		if ( is_singular() ) : ?>
	<div id="fh5co-title-box" style="background-image: url(<?php the_post_thumbnail_url(); ?>); background-position: 50% -90.5px;" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="page-title">
	<?php $avatar_url = get_avatar_url(get_the_author_meta( 'ID' ), array('size' => 450)); ?>
        <img src="<?php echo esc_url( $avatar_url ); ?>" alt="<?php the_author();?>">
		<span><?php echo get_the_time('M d, y'); ?></span>
		<p class="author"><?php the_author();?></p>
		<p class="author-email"><?php global $current_user;
	  get_currentuserinfo();
	  echo $current_user->user_email; ?></p>
        <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
    </div>
</div>
	
			
	<?php	else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				theabhinews_in_posted_on();
				theabhinews_in_posted_by();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	
	<div id="fh5co-single-content" class="container-fluid pb-4 pt-4 paddding">
    <div class="container paddding">
        <div class="row mx-0">
            <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
	<div class="entry-content">
		<?php
		the_content( sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'theabhinews-in' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'theabhinews-in' ),
			'after'  => '</div>',
		) );
		?>

	</div><!-- .entry-content -->
	<?php if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif; ?>
		</div>
		<?php get_sidebar(); ?>
		</div>
		</div>
		</div>
	<footer class="entry-footer">
		<?php theabhinews_in_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
