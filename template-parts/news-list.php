<?php
/**
 * Template Name: News List
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package theabhinews.in
 */
get_header();

?>
<?php 
    global $post;
    $cats = $post_slug=$post->post_name;
?>

<div class="container-fluid pb-4 pt-4 paddding">
    <div class="container paddding">
        <div class="row mx-0">
            <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                <div>
                    <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4"><?php the_title();?></div>
                </div>
                <?php query_posts("post_type=news&field=slug&post_status=publish&posts_per_page=-1&category_name=$cats"); ?>
        <?php if(have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?> 
                <div class="row pb-4">
                    <div class="col-md-5">
                        <div class="fh5co_hover_news_img">
                            <div class="fh5co_news_img">
                            <?php if ( has_post_thumbnail() ) { ?>
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt=""/>
                            <?php } else { ?>    
                                <img src="/wp-content/themes/tan/images/zack-minor-15104.jpg" alt=""/>
                            <?php } ?>
                            </div>
                            <div></div>
                        </div>
                    </div>
                    <div class="col-md-7 animate-box">
                        <a href="<?php the_permalink();?>" class="fh5co_magna py-2"><?php the_title(); ?></a> <a href="<?php the_permalink();?>" class="fh5co_mini_time py-3"> <?php the_author();?> -
                        <?php echo get_the_time('M d, Y'); ?> </a>
                        <div class="fh5co_consectetur"> 
                        <?php the_excerpt(); ?>
                        </div>
                    </div>
                </div>
                <?php endwhile;
                endif; ?>
                
             </div>   
            <?php get_sidebar();?>
        <div class="row mx-0">
            <div class="col-12 text-center pb-4 pt-4">
                <a href="#" class="btn_mange_pagging"><i class="fa fa-long-arrow-left"></i>&nbsp;&nbsp; Previous</a>
                <a href="#" class="btn_pagging">1</a>
                <a href="#" class="btn_pagging">2</a>
                <a href="#" class="btn_pagging">3</a>
                <a href="#" class="btn_pagging">...</a>
                <a href="#" class="btn_mange_pagging">Next <i class="fa fa-long-arrow-right"></i>&nbsp;&nbsp; </a>
             </div>
        </div>
    </div>
</div>
<?php 

get_footer();
