import 'bootstrap';
import 'customizer.js';
import 'jquery.stellar.min.js';
import 'jquery.waypoints.min.js';
import 'navigation.js';
import 'owl.carousel.min.js';
import 'main.js';